import { ActivityIndicator, View } from "react-native";
import React from "react";


export default function ListFooter() {
    return (
        <View
            style={{
                position: 'relative',
                width: '100%',
                height: '100%',
                paddingVertical: 20,
            }}
        >
            <ActivityIndicator animating size="large" />
        </View>
    )
}