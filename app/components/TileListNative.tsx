import { TileItem, TileInterface} from "./TileItem";
import ListFooter from "./ListFooter";
import React  from "react";
import { FlatList } from "react-native";

export interface TileList {
    pageSize: number
    tiles: Array<TileInterface>,
    loadTiles: () => void
}

export default function TileListNative({ tiles, loadTiles, pageSize}: TileList) {
    return (
        <FlatList
            data={tiles}
            renderItem={({ item }) => <TileItem tile={item} />}
            keyExtractor={item => item.id}
            onEndReached={() => loadTiles()}
            onEndReachedThreshold={0.5}
            initialNumToRender={pageSize}
            ListFooterComponent={<ListFooter/>}
        />
    );
}