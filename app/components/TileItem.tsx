import {StyleSheet, Image, Linking, TouchableOpacity } from "react-native";
import React from "react";

const styles = StyleSheet.create({
    tileImage: {
        width: '100%',
        height: 350,
        resizeMode: 'cover',
        maxWidth: 600,
        alignSelf: 'center'
    },
});

const loadInBrowser = (url) => {
    Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
};

export interface TileInterface {
    id: string,
    url: string,
    tileImage: string,
}

export interface TileItemProps {
   tile: TileInterface
}

export const TileItem = ({tile}: TileItemProps) => (
    <TouchableOpacity onPress={() => loadInBrowser(tile.url)}>
        <Image source={{uri : tile.tileImage }} style={styles.tileImage} />
    </TouchableOpacity>
);