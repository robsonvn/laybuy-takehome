/**
 * Interface for the tile schema
 */
export default interface TileInterface {
    id: String,
    attributes: {
        name: String,
        url: String,
        tileImage: {
            url: String,
        }
    }
}