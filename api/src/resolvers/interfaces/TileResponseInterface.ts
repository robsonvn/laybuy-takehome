import TileInterface from "./TileInterface";

/**
 * Interface for the endpoint /tiles response
 */
export default interface TileResponseInterface {
    data: Array<TileInterface>
    meta: {
        totalPages: number,
        count: number,
        page: number
    },
}
