import { gql } from 'apollo-server';

export default gql`
    type Query {
        tiles (pageSize: Int, pageNumber: Int): [Tile!]!
    }

    type Tile {
        id: ID!
        name: String!
        url: String!
        tileImage: String!
    }
`;
