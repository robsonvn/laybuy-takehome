# Laybuy Takehome

First I would like to apologise for making you installing the libraries on local machine but running [Expo](https://expo.io) on Docker it's very tricky and time consuming to setup.

The project is split in two part

* **API** - wraps the Laybuy REST API into a GraphQL schema.
* **App** - is a simple project built using Expo, React Native, and Typescript that consumes the GraphQL API that simply fetch the tiles, paginates and displays in a infinity scroll component.


## API

Install project dependencies

```bash
cd api/
cp .env.example .env
yarn install
```


Build and run

```bash
NODE_ENV=production yarn build && yarn start
```


Alternatively, the development environment is also available with hot module replacement and GraphQL playground

```bash
NODE_ENV=development yarn build 
```

The Playground will be available at http://localhost:4000


## App


Install the dependencies and install the `expo-cli` globally

```bash
cd app/
sudo npm install -g expo-cli
```

Install the dependencies.

```bash
yarn install
```

Start the project

```bash
yarn start
```

The project URL will be given by the `expo-cli`

If you want to test the application mobile, please download the `Expo` on Android or App Store and scan the QR Code.

You also will need to update the `apiUrl`  with the correct GraphQL API at `app/variable.ts`

More info at: https://docs.expo.io/versions/latest/get-started/installation/